module Validate
(
    checkCourses
) where

import Structures
    ( Course(prerequisites, corequisites) )

subset :: (Foldable t, Eq a) => [a] -> t a -> Bool
subset a b = all (`elem` b) a

hasRequisites :: Structures.Course -> [Structures.Course] -> Bool
hasRequisites course concurrentCourses = subset concurrentCourses (prerequisites course ++ corequisites course)

checkCourses :: [Structures.Course] -> Bool
checkCourses [] = True
checkCourses [x] = null (prerequisites x) && null (corequisites x)
checkCourses (x:xs) = hasRequisites x xs && checkCourses xs
