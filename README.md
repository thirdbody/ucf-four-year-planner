# ucf-four-year-planner

Checks a provided 4-year plan for common issues (i.e. missing prerequisites, low credits, etc.). Intended for use at UCF.