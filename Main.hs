import System.IO ( hFlush, stdout )
import Data.Maybe ( Maybe, isJust, fromJust )
import Text.Read ( readMaybe )

import Parse ( parseCourse )
import Structures ( Course(..) )
import Validate ( checkCourses )

prompt :: String -> IO String
prompt text = do
    putStr text
    hFlush stdout
    getLine

findCourse :: String -> [Course] -> Maybe Course
findCourse _ [] = Nothing
findCourse courseName [x] = if ( division x ++ show (number x) ) == courseName then Just x else findCourse courseName (prerequisites x ++ corequisites x)
findCourse courseName (x:xs)
    | isJust findCourseHead = findCourseHead
    | otherwise = findCourseTail
    where findCourseHead = findCourse courseName [x]
          findCourseTail = findCourse courseName xs

promptForExtraCourses :: String -> String -> [Course] -> [Course] -> IO [Course]
promptForExtraCourses promptText forCourse currentList allCoursesSoFar = do
    nextReq <- prompt ("Enter next " ++ promptText ++ " for " ++ forCourse ++ ". Press ENTER if no additional " ++ promptText ++ "s: ")
    if null nextReq
        then return currentList
        else do
            let existsAlready = findCourse nextReq (currentList ++ allCoursesSoFar)
            if isJust existsAlready
                then do
                    putStrLn ("You've already inputted the data for " ++ nextReq ++ ". That data will be re-applied.")
                    promptForExtraCourses promptText forCourse (currentList ++ [fromJust existsAlready]) allCoursesSoFar
                else do
                    nextLevelPrereqs <- promptForExtraCourses "prerequisite" nextReq [] allCoursesSoFar           
                    nextLevelCoreqs <- promptForExtraCourses "corequisite" nextReq [] allCoursesSoFar          
                    let newList = currentList ++ [parseCourse nextReq nextLevelPrereqs nextLevelCoreqs]
                    promptForExtraCourses promptText forCourse newList allCoursesSoFar

validate :: [Course] -> String 
validate courses
    | result = "Schedule is valid"
    | otherwise = "Schedule is not valid"
    where result = Validate.checkCourses courses

mainLoop :: [Course] -> IO ()
mainLoop courses = do
    nextCourse <- prompt "Enter next course or nothing to validate schedule: "
    if null nextCourse
        then do
            writeFile "./courses.txt" (show courses)
            putStrLn (validate courses)
        else do
            prereqs <- promptForExtraCourses "prerequisite" nextCourse [] courses
            coreqs <- promptForExtraCourses "corequisite" nextCourse [] courses     
            let newCourses = courses ++ [parseCourse nextCourse prereqs coreqs]
            mainLoop newCourses

main :: IO ()
main = do
    savedCourses <- readFile "./courses.txt"
    let parsedCourses = readMaybe savedCourses :: Maybe [Course]
    maybe (mainLoop []) mainLoop parsedCourses
